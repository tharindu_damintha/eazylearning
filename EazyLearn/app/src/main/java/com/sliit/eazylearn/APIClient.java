package com.sliit.eazylearn;

import com.sliit.eazylearn.componentfour.data.model.ServerObject;

import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface APIClient {
    @Multipart
    @POST("/generateQuestion")
    Call<List<ServerObject>> generateQuestions(@Part("summarize") RequestBody summarize);

}
