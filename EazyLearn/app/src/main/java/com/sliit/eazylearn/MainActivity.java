package com.sliit.eazylearn;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.sliit.eazylearn.componentfour.BaseActivity;
import com.sliit.eazylearn.componentfour.QuizHomeActivity;
import com.sliit.eazylearn.componentone.MainActivityImageClassify;
import com.sliit.eazylearn.componentthree.ARActivity;
import com.sliit.eazylearn.componenttwo.MainActivityVideoUrls;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ((EditText)findViewById(R.id.etIp)).setText(MyApp.BASE_URL);
        requestPermission();
    }

    public void onComponentFourClick(View view) {
        QuizHomeActivity.startActivity(this);
    }

    public void onComponentOneClick(View view) {
        MainActivityImageClassify.startActivity(this);
    }

    public void onComponentThreeClick(View view) {
        ARActivity.startActivity(this);
    }

    public void onComponentTwoClick(View view) {
        MainActivityVideoUrls.startActivity(this);
    }

    public void onUpdateClick(View view) {
        MyApp.BASE_URL =((EditText)findViewById(R.id.etIp)).getText().toString();
        Toast.makeText(this, "Updated", Toast.LENGTH_SHORT).show();
    }
}
