package com.sliit.eazylearn;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.facebook.stetho.Stetho;
import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.sliit.eazylearn.componentfour.data.MyDatabase;
import com.sliit.eazylearn.componentfour.data.model.Answer;
import com.sliit.eazylearn.componentfour.data.model.MyQuestion;
import com.sliit.eazylearn.componentfour.data.model.QuestionWithAnswers;
import com.sliit.eazylearn.componentone.NetworkClient;

import java.util.ArrayList;
import java.util.concurrent.Executors;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MyApp extends Application {
    public static  String BASE_URL = "http://192.168.8.102:5002/";
    private static MyApp INSTANCE = null;
    private MyDatabase myDatabase = null;

    public static MyApp getInstance() {
        return INSTANCE;
    }

    private SharedPreferences sharedPreferences;

    @Override
    public void onCreate() {
        super.onCreate();
        INSTANCE = this;
        sharedPreferences = getSharedPreferences("MyPreferences", Context.MODE_PRIVATE);
        Stetho.initializeWithDefaults(this);

        myDatabase = Room.databaseBuilder(this, MyDatabase.class, "QuestionDB")
                .allowMainThreadQueries()
                .addCallback(new RoomDatabase.Callback() {
                    @Override
                    public void onCreate(@NonNull SupportSQLiteDatabase db) {
                        super.onCreate(db);
                        Executors.newSingleThreadScheduledExecutor().execute(new Runnable() {
                            @Override
                            public void run() {
                                insertMasterData();
                            }
                        });

                    }

                    @Override
                    public void onOpen(@NonNull SupportSQLiteDatabase db) {
                        super.onOpen(db);
                    }

                    @Override
                    public void onDestructiveMigration(@NonNull SupportSQLiteDatabase db) {
                        super.onDestructiveMigration(db);
                    }
                })
                .build();
    }

    public MyDatabase getDatabase() {
        return myDatabase;
    }

    public void insertMasterData() {
        QuestionWithAnswers questionWithAnswers = new QuestionWithAnswers();
        questionWithAnswers.myQuestion = new MyQuestion("Which of the following fruit has no seed?");
        questionWithAnswers.answers = new ArrayList<>();
        int rowId = (int) getDatabase().getMasterDAO().insert(questionWithAnswers.myQuestion);
        questionWithAnswers.answers.add(new Answer("Cucumber", rowId, true));
        questionWithAnswers.answers.add(new Answer("Mango", rowId));
        questionWithAnswers.answers.add(new Answer("Apple", rowId));
        getDatabase().getMasterDAO().insert(questionWithAnswers.answers);


        questionWithAnswers.myQuestion = new MyQuestion("______Are the oldest and extensively used nuts in the world");
        questionWithAnswers.answers = new ArrayList<>();
        rowId = (int) getDatabase().getMasterDAO().insert(questionWithAnswers.myQuestion);
        questionWithAnswers.answers.add(new Answer("Almand", rowId, true));
        questionWithAnswers.answers.add(new Answer("Mango", rowId));
        questionWithAnswers.answers.add(new Answer("Apple", rowId));
        getDatabase().getMasterDAO().insert(questionWithAnswers.answers);

        questionWithAnswers.myQuestion = new MyQuestion("Banana oil is ade from ______");
        questionWithAnswers.answers = new ArrayList<>();
        rowId = (int) getDatabase().getMasterDAO().insert(questionWithAnswers.myQuestion);
        questionWithAnswers.answers.add(new Answer("Banana", rowId, true));
        questionWithAnswers.answers.add(new Answer("Petroleum", rowId));
        questionWithAnswers.answers.add(new Answer("orange", rowId));
        getDatabase().getMasterDAO().insert(questionWithAnswers.answers);
    }

    public APIClient getApiClient(){
        return getRetrofitClient(BASE_URL).create(APIClient.class);
    }

    private static Retrofit retrofit;
    public static Retrofit getRetrofitClient(String baseUrl) {
        if (retrofit == null) {
            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .addNetworkInterceptor(new StethoInterceptor())
                    .build();
            retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)

                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

    public void storeHighScore(int score){
        sharedPreferences.edit().putInt("highScore", score).apply();
    }

    public int getHighScore() {
        return sharedPreferences.getInt("highScore", 0);
    }
}
