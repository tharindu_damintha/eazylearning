package com.sliit.eazylearn.componentfour;

import android.util.Log;

import com.sliit.eazylearn.MyApp;
import com.sliit.eazylearn.componentfour.data.model.Answer;
import com.sliit.eazylearn.componentfour.data.model.MyQuestion;
import com.sliit.eazylearn.componentfour.data.model.QuestionWithAnswers;
import com.sliit.eazylearn.componentfour.data.model.RequestGenerateQuestion;
import com.sliit.eazylearn.componentfour.data.model.ServerObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ApiManager {

    public interface OnResultLoadedListener {
        void onSuccess(List<ServerObject> questions);

        void onFailed();
    }

    public static void generateQuestions(String summarize, OnResultLoadedListener onResultLoadedListener) {
        RequestBody summarizeBody = RequestBody.create(MediaType.parse("text/plain"), summarize);
        MyApp.getInstance().getApiClient().generateQuestions(summarizeBody).enqueue(new Callback<List<ServerObject>>() {
            @Override
            public void onResponse(Call<List<ServerObject>> call, Response<List<ServerObject>> response) {
                if(response.isSuccessful() && response.body()!= null && !response.body().isEmpty()){
                    for(ServerObject nextLine : response.body()) {
                        MyQuestion myQuestion = new MyQuestion();
                        myQuestion.setQuestion(nextLine.questionText);
                        String correctAnswer = nextLine.correctAnswer;
                        QuestionWithAnswers questionWithAnswer = new QuestionWithAnswers();
                        questionWithAnswer.myQuestion = myQuestion;
                        if (MyApp.getInstance().getDatabase().getMasterDAO().getQuestionByQuestionText(questionWithAnswer.myQuestion.question) == null) {
                            long qId = MyApp.getInstance().getDatabase().getMasterDAO().insert(questionWithAnswer.myQuestion);

                            questionWithAnswer.answers = new ArrayList<>();
                            for (int i = 2; i < nextLine.answers.size(); i++) {
                                Answer answer = new Answer();
                                answer.setAnswer(nextLine.answers.get(i));
                                answer.setCorrectAnswer(correctAnswer.equalsIgnoreCase(answer.getAnswer()));
                                answer.setQuestionId((int) qId);
                                questionWithAnswer.answers.add(answer);
                            }
                            MyApp.getInstance().getDatabase().getMasterDAO().insert(questionWithAnswer.answers);
                        }
                    }
                    onResultLoadedListener.onSuccess(response.body());
                }else{
                    onResultLoadedListener.onFailed();
                }
            }

            @Override
            public void onFailure(Call<List<ServerObject>> call, Throwable t) {
                onResultLoadedListener.onFailed();
            }
        });


    }
}
