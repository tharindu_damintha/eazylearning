package com.sliit.eazylearn.componentfour;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.sliit.eazylearn.MyApp;
import com.sliit.eazylearn.R;
import com.sliit.eazylearn.componentfour.data.model.Answer;
import com.sliit.eazylearn.componentfour.data.model.QuestionWithAnswers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

public class QuizActivity extends AppCompatActivity {

    public static final String EXTRA_SCORE = "extraScore";
    private static final long COUNTDOWN_IN_MILLIS = 30000;

    private static final String KEY_SCORE = "keyScore";
    private static final String KEY_QUESTION_COUNT = "keyQuestionCount";
    private static final String KEY_MILLIS_LEFT = "keyMillisLeft";
    private static final String KEY_ANSWERED = "keyAnswered";
    private static final String KEY_QUESTION_LIST = "keyQuestionList";


    private TextView textViewQuestion;
    private TextView textViewScore;
    private TextView textViewQuestionCount;
    private TextView textViewDifficulty;
    private TextView textViewCountDown;
    private RadioGroup rbGroup;
    private Button buttonConfirmNext;

    private ColorStateList textColorDefaultcd;

    private CountDownTimer countDownTimer;
    private long timeLeftInMillis;

    private List<QuestionWithAnswers> questionWithAnswers;
    private int questionCounter;
    private int questionCountTotal;
    private QuestionWithAnswers currentQuestion;

    private int score;
    private boolean answered;

    private long backPressedTime;

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, QuizActivity.class);
        context.startActivity(intent);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);

        textViewQuestion = findViewById(R.id.text_view_question);
        textViewScore = findViewById(R.id.text_view_score);
        textViewQuestionCount = findViewById(R.id.text_view_question_count);
        textViewDifficulty = findViewById(R.id.text_view_difficulty);
        textViewCountDown = findViewById(R.id.text_view_countdown);
        rbGroup = findViewById(R.id.radio_group);
        buttonConfirmNext = findViewById(R.id.button_confirm_next);

        textColorDefaultcd = textViewCountDown.getHintTextColors();

        questionWithAnswers = MyApp.getInstance().getDatabase().getMasterDAO().getQuestionsWithAnswers();
        Collections.shuffle(questionWithAnswers);
        questionWithAnswers = pickQuestions(questionWithAnswers);
        questionCountTotal = questionWithAnswers.size();
        showNextQuestion();
        buttonConfirmNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!answered) {
                    int selectedId = rbGroup.getCheckedRadioButtonId();

                    // find the radiobutton by returned id
                    RadioButton rdoAnswer = rbGroup.findViewById(selectedId);


                    if (rdoAnswer != null) {
                        checkAnswer();
                    } else {
                        Toast.makeText(QuizActivity.this, "Please select an answer", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    showNextQuestion();
                }

            }

        });
    }

    private List<QuestionWithAnswers> pickQuestions(List<QuestionWithAnswers> questionWithAnswers) {
        List<QuestionWithAnswers> newList = new ArrayList<>();
        for (int i = 0; i < (questionWithAnswers.size() > 20 ? 20 : questionWithAnswers.size());i++){
            newList.add(questionWithAnswers.get(i));
        }
        return newList;
    }

    private void showNextQuestion() {

        if (questionCounter < questionCountTotal) {
            rbGroup.removeAllViews();
            currentQuestion = questionWithAnswers.get(questionCounter);
            textViewQuestion.setText(currentQuestion.myQuestion.question);
            for (Answer answer : currentQuestion.answers) {
                RadioButton radioButton = new RadioButton(this);
                radioButton.setTag(answer);
                radioButton.setText(answer.getAnswer());
                rbGroup.addView(radioButton);
            }

            questionCounter++;
            textViewQuestionCount.setText("Question: " + questionCounter + "/" + questionCountTotal);
            answered = false;
            buttonConfirmNext.setText("Confirm");

            startCountDown();

        } else {
            finishQuiz();
        }
    }

    private void startCountDown() {
        countDownTimer = new CountDownTimer(COUNTDOWN_IN_MILLIS, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                timeLeftInMillis = millisUntilFinished;
                updateCountDownText();
            }

            @Override
            public void onFinish() {
                timeLeftInMillis = 0;
                updateCountDownText();
                checkAnswer();
            }
        }.start();
    }

    private void updateCountDownText() {
        int minutes = (int) (timeLeftInMillis / 1000) / 60;
        int seconds = (int) (timeLeftInMillis / 1000) % 60;

        String timeFormatted = String.format(Locale.getDefault(), "%02d:%02d", minutes, seconds);

        textViewCountDown.setText(timeFormatted);

        if (timeLeftInMillis < 10000) {
            textViewCountDown.setTextColor(Color.RED);
        } else {
            textViewCountDown.setTextColor(textColorDefaultcd);
        }
    }

    private void checkAnswer() {
        answered = true;

        countDownTimer.cancel();
        RadioButton rbSelected = findViewById(rbGroup.getCheckedRadioButtonId());
        if(rbSelected == null) return;

        Answer answerNr = (Answer) rbSelected.getTag();

        if (answerNr.isCorrectAnswer()) {
            score++;
            textViewScore.setText("Score: " + score);
        }
        showSolution();
    }

    private void showSolution() {
        for (int i = 0; i < rbGroup.getChildCount(); i++) {
            RadioButton radioButton = (RadioButton) rbGroup.getChildAt(i);
            radioButton.setTextColor(Color.RED);
            if (radioButton.isChecked() && ((Answer) radioButton.getTag()).isCorrectAnswer()) {
                radioButton.setTextColor(Color.GREEN);
                textViewQuestion.setText("Answer " + (i + 1) + " is correct ");
            }
        }

        if (questionCounter < questionCountTotal) {
            buttonConfirmNext.setText("Next");
        } else {
            buttonConfirmNext.setText("Finish");
        }

    }

    private AlertDialog alertDialog;
    private void finishQuiz() {
        int currentHighScore = MyApp.getInstance().getHighScore();
        boolean youWon = false;
        if (score > currentHighScore) {
            youWon = true;
            currentHighScore = score;
            MyApp.getInstance().storeHighScore(score);
        }

        View view = LayoutInflater.from(this).inflate(R.layout.layout_winn, null, false);
        if(youWon){
            view.findViewById(R.id.imgWon).setVisibility(View.VISIBLE);
            Glide.with(this).load(R.drawable.ic_cup_anim).into(((ImageView)view.findViewById(R.id.imgWon)));
        }
        view.findViewById(R.id.buttonOk).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(alertDialog!= null && alertDialog.isShowing()){
                    alertDialog.dismiss();
                    finish();
                }
            }
        });
        view.findViewById(R.id.buttonRetry).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(alertDialog!= null && alertDialog.isShowing()){
                    alertDialog.dismiss();
                    finish();
                    startActivity(getIntent());
                }
            }
        });
        ((TextView)view.findViewById(R.id.txtHighScore)).setText("High score: " + currentHighScore);
        ((TextView)view.findViewById(R.id.txtYourScore)).setText("Your score: " + score);
        alertDialog =  new AlertDialog.Builder(this)
                .setView(view)
                .setTitle("Finished")
                .create();
        alertDialog.show();
    }

    @Override
    public void onBackPressed() {
        if (backPressedTime + 2000 > System.currentTimeMillis()) {
            finishQuiz();
        } else {
            Toast.makeText(this, "press back again to finish", Toast.LENGTH_SHORT).show();
        }
        backPressedTime = System.currentTimeMillis();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }

    }
}
