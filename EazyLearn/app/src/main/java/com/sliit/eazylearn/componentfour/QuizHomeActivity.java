package com.sliit.eazylearn.componentfour;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.opencsv.CSVReader;
import com.sliit.eazylearn.MyApp;
import com.sliit.eazylearn.R;
import com.sliit.eazylearn.componentfour.data.model.Answer;
import com.sliit.eazylearn.componentfour.data.model.MyQuestion;
import com.sliit.eazylearn.componentfour.data.model.QuestionWithAnswers;
import com.sliit.eazylearn.componentfour.data.model.ServerObject;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class QuizHomeActivity extends BaseActivity {


    private static final int ACTIVITY_CHOOSE_FILE1 = 1000;

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, QuizHomeActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        requestPermission();
    }

    public void onImportClick(View view) {
//        if (!hasPermission())
//            requestPermission();
//        else
//            selectCSVFile();
        ApiManager.generateQuestions("The Cobra is the longest venomous snake in the world. \n" +
                "It is famous for its fierceness and is extremely dangerous.\n" +
                "The Cobra lives in Southeast Asia including parts of India and other countries such as Burma, Thailand, Indonesia, and the Philippines. \n" +
                "They like to live in forests and near water. They can swim well and can move quickly in trees and on land.\n" +
                "Cobras typically grow to around 13 feet long, but they have been known to grow as long as 18 feet. \n" +
                "The color of the king cobra is black, tan, or dark green with yellow bands down the length of the body. \n" +
                "The belly is cream colored with black bands\n", new ApiManager.OnResultLoadedListener() {
            @Override
            public void onSuccess(List<ServerObject> questions) {
                new AlertDialog.Builder(QuizHomeActivity.this)
                        .setTitle("Success")
                        .setMessage("Question generation successfully")
                        .setPositiveButton("OK", (dialogInterface, i) -> dialogInterface.dismiss())
                        .show();
            }

            @Override
            public void onFailed() {
                new AlertDialog.Builder(QuizHomeActivity.this)
                        .setTitle("Failed")
                        .setMessage("Question generation failed")
                        .setPositiveButton("OK", (dialogInterface, i) -> dialogInterface.dismiss())
                        .show();
            }
        });
    }

    public void openQuiz(View view) {
        QuizActivity.startActivity(this);
    }

//
//    private void selectCSVFile() {
//        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
//        intent.addCategory(Intent.CATEGORY_OPENABLE);
//        intent.setType("*/*");
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
//            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);
//        }
//        startActivityForResult(Intent.createChooser(intent, "Open CSV"), ACTIVITY_CHOOSE_FILE1);
//    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (resultCode == Activity.RESULT_OK) {
//            if (data != null) {
//                String uri = FileUtils.getPath(this, data.getData());
//                readCSVandStore(new File(uri));
//            } else {
//                Toast.makeText(this, "File loading error                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           ", Toast.LENGTH_SHORT).show();
//            }
//        }
//    }

//    private void readCSVandStore(File file) {
//        try {
//            CSVReader reader = new CSVReader(new FileReader(file));
//            String[] nextLine;
//            List<QuestionWithAnswers> questionWithAnswers = new ArrayList<>();
//            while ((nextLine = reader.readNext()) != null) {
//                // nextLine[] is an array of values from the line
//                if (nextLine.length > 0 && !TextUtils.isEmpty(nextLine[0])) {
//                    MyQuestion myQuestion = new MyQuestion();
//                    myQuestion.setQuestion(nextLine[0]);
//                    String correctAnswer = nextLine[1];
//                    QuestionWithAnswers questionWithAnswer = new QuestionWithAnswers();
//                    questionWithAnswer.myQuestion = myQuestion;
//                    if (MyApp.getInstance().getDatabase().getMasterDAO().getQuestionByQuestionText(questionWithAnswer.myQuestion.question) == null) {
//                        long qId = MyApp.getInstance().getDatabase().getMasterDAO().insert(questionWithAnswer.myQuestion);
//
//                        questionWithAnswer.answers = new ArrayList<>();
//                        for (int i = 2; i < nextLine.length; i++) {
//                            Answer answer = new Answer();
//                            answer.setAnswer(nextLine[i]);
//                            answer.setCorrectAnswer(correctAnswer.equalsIgnoreCase(answer.getAnswer()));
//                            answer.setQuestionId((int) qId);
//                            questionWithAnswer.answers.add(answer);
//                        }
//                        questionWithAnswers.add(questionWithAnswer);
//
//                        MyApp.getInstance().getDatabase().getMasterDAO().insert(questionWithAnswer.answers);
//                    }
//                }
//            }
//            new AlertDialog.Builder(this)
//                    .setTitle("Success")
//                    .setMessage("Imported message successfully")
//                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialogInterface, int i) {
//                            dialogInterface.dismiss();
//                        }
//                    })
//                    .show();
//            Log.d("questionWithAnswers", String.valueOf(questionWithAnswers.size()));
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//    }
}
