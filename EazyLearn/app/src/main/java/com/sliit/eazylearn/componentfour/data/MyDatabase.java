package com.sliit.eazylearn.componentfour.data;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.sliit.eazylearn.componentfour.data.daos.MasterDAO;
import com.sliit.eazylearn.componentfour.data.model.Answer;
import com.sliit.eazylearn.componentfour.data.model.Level;
import com.sliit.eazylearn.componentfour.data.model.MyQuestion;


@Database(entities = {MyQuestion.class, Answer.class, Level.class}, version = 1)
public abstract class MyDatabase extends RoomDatabase {
    public abstract MasterDAO getMasterDAO();
}