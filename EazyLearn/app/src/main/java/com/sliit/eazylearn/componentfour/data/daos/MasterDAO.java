package com.sliit.eazylearn.componentfour.data.daos;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.sliit.eazylearn.componentfour.data.model.Answer;
import com.sliit.eazylearn.componentfour.data.model.MyQuestion;
import com.sliit.eazylearn.componentfour.data.model.QuestionWithAnswers;

import java.util.List;

@Dao
public interface MasterDAO {
    @Insert
    long insert(MyQuestion question);

    @Insert
    void insert(List<Answer> answers);

    @Query("SELECT * FROM MyQuestion")
    List<MyQuestion> getQuestions();

    @Query("SELECT * FROM Answer")
    List<Answer> getAnswers();

    @Query("SELECT * from MyQuestion")
    List<QuestionWithAnswers> getQuestionsWithAnswers();

    @Query("SELECT * FROM MyQuestion WHERE question = :question")
    MyQuestion getQuestionByQuestionText(String question);
}
