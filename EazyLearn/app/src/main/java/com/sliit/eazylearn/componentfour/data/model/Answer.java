package com.sliit.eazylearn.componentfour.data.model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Answer {
    @PrimaryKey(autoGenerate = true)
    private int answerId;

    public Answer(int answerId, int questionId, String answer, boolean isCorrectAnswer) {
        this.answerId = answerId;
        this.questionId = questionId;
        this.answer = answer;
        this.isCorrectAnswer = isCorrectAnswer;
    }

    private int questionId;
    private String answer;
    private boolean isCorrectAnswer;

    public Answer(){

    }

    public Answer(int answerId, String answer) {
        this.answerId = answerId;
        this.answer = answer;
    }

    public Answer(String answer, int questionId, boolean isCorrectAnswer) {
        this.answer = answer;
        this.questionId = questionId;
        this.isCorrectAnswer = isCorrectAnswer;
    }

    public Answer(String answer, int questionId) {
        this.answer = answer;
        this.questionId = questionId;
    }

    public int getAnswerId() {
        return answerId;
    }

    public void setAnswerId(int answerId) {
        this.answerId = answerId;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }


    public boolean isCorrectAnswer() {
        return isCorrectAnswer;
    }

    public void setCorrectAnswer(boolean correctAnswer) {
        isCorrectAnswer = correctAnswer;
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }
}
