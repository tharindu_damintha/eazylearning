package com.sliit.eazylearn.componentfour.data.model;

import androidx.room.Embedded;
import androidx.room.Relation;

import java.util.List;

public class QuestionWithAnswers {
    @Embedded
    public MyQuestion myQuestion;

    @Relation(parentColumn = "id", entityColumn = "questionId", entity = Answer.class)
    public List<Answer> answers;

}
