package com.sliit.eazylearn.componentfour.data.model;

import java.util.List;

public class ServerObject {
    public String questionText;
    public String correctAnswer;
    public List<String> answers;
}
