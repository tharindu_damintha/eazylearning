package com.sliit.eazylearn.componentone;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.sliit.eazylearn.MyApp;
import com.sliit.eazylearn.R;
import com.sliit.eazylearn.componentfour.ApiManager;
import com.sliit.eazylearn.componentfour.data.model.ServerObject;
import com.sliit.eazylearn.componentone.model.DataModel;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

public class Description extends AppCompatActivity {

    TextView textView;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_description);
        textView = findViewById(R.id.textView);
        progressBar = findViewById(R.id.progressBar);
        Intent intent = getIntent();
        String selectedAnimal = intent.getStringExtra("selectedAnimal");
        if(selectedAnimal.equalsIgnoreCase("cow")) {
            new HttpGetRequest().execute(MyApp.BASE_URL + "cow");
        }
        if(selectedAnimal.equalsIgnoreCase("fish")) {
            new HttpGetRequest().execute(MyApp.BASE_URL + "fish");
        }
        if(selectedAnimal.equalsIgnoreCase("parrot")) {
            new HttpGetRequest().execute(MyApp.BASE_URL + "parrot");
        }
        if(selectedAnimal.equalsIgnoreCase("butterfly")) {
            new HttpGetRequest().execute(MyApp.BASE_URL + "butterfly");
        }
        if(selectedAnimal.equalsIgnoreCase("dinosaur")) {
            new HttpGetRequest().execute(MyApp.BASE_URL + "dinosaur");
        }
        if(selectedAnimal.equalsIgnoreCase("betal")) {
            new HttpGetRequest().execute(MyApp.BASE_URL + "betal");
        }
        if(selectedAnimal.equalsIgnoreCase("jack")) {
            new HttpGetRequest().execute(MyApp.BASE_URL + "jack");
        }
        if(selectedAnimal.equalsIgnoreCase("moringa")) {
            new HttpGetRequest().execute(MyApp.BASE_URL + "moringa");
        }
        if(selectedAnimal.equalsIgnoreCase("curry")) {
            new HttpGetRequest().execute(MyApp.BASE_URL + "curry");
        }
        if(selectedAnimal.equalsIgnoreCase("papaya")) {
            new HttpGetRequest().execute(MyApp.BASE_URL + "papaya");
        }
    }

    public void onGenerateQuestionsClick(View view) {
        if(TextUtils.isEmpty(textView.getText())){
            Toast.makeText(this, "No summary found", Toast.LENGTH_LONG).show();
            return;
        }
        ApiManager.generateQuestions(textView.getText().toString(), new ApiManager.OnResultLoadedListener() {
            @Override
            public void onSuccess(List<ServerObject> questions) {
                new AlertDialog.Builder(Description.this)
                        .setTitle("Success")
                        .setMessage("Question generation successfully")
                        .setPositiveButton("OK", (dialogInterface, i) -> dialogInterface.dismiss())
                        .show();
            }

            @Override
            public void onFailed() {
                new AlertDialog.Builder(Description.this)
                        .setTitle("Failed")
                        .setMessage("Question generation failed")
                        .setPositiveButton("OK", (dialogInterface, i) -> dialogInterface.dismiss())
                        .show();
            }
        });

    }

    public class HttpGetRequest extends AsyncTask<String, Void, String> {
        public static final String REQUEST_METHOD = "GET";
        public static final int READ_TIMEOUT = 35000;
        public static final int CONNECTION_TIMEOUT = 35000;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
            String stringUrl = params[0];
            String result;
            String inputLine;
            try {
                //Create a URL object holding our url
                URL myUrl = new URL(stringUrl);
                //Create a connection
                HttpURLConnection connection = (HttpURLConnection)
                        myUrl.openConnection();
                //Set methods and timeouts
                connection.setRequestMethod(REQUEST_METHOD);
                connection.setReadTimeout(READ_TIMEOUT);
                connection.setConnectTimeout(CONNECTION_TIMEOUT);
                //Connect to our url
                connection.connect();
                //Create a new InputStreamReader
                InputStreamReader streamReader = new
                        InputStreamReader(connection.getInputStream());
                //Create a new buffered reader and String Builder
                BufferedReader reader = new BufferedReader(streamReader);
                StringBuilder stringBuilder = new StringBuilder();
                //Check if the line we are reading is not null
                while ((inputLine = reader.readLine()) != null) {
                    stringBuilder.append(inputLine);
                }
                //Close our InputStream and Buffered reader
                reader.close();
                streamReader.close();
                //Set our result equal to our stringBuilder
                result = stringBuilder.toString();
            } catch (IOException e) {
                e.printStackTrace();
                result = null;
            }
            return result;
        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressBar.setVisibility(View.GONE);
            DataModel dataModel = new Gson().fromJson(result, DataModel.class);
            if (dataModel != null) {
                textView.setText(dataModel.getData());
            }
        }
    }
}
