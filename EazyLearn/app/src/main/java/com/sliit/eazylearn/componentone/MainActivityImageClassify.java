package com.sliit.eazylearn.componentone;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import com.sliit.eazylearn.BuildConfig;
import com.sliit.eazylearn.R;
import com.sliit.eazylearn.componentone.enums.ImagePickerEnum;
import com.sliit.eazylearn.componentone.listeners.IImagePickerLister;
import com.sliit.eazylearn.componentone.utils.FileUtils;
import com.sliit.eazylearn.componentone.utils.UiHelper;
import com.sliit.eazylearn.componenttwo.SecondActivityVideoUrls;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MainActivityImageClassify extends AppCompatActivity implements IImagePickerLister {

    private static final int CAMERA_ACTION_PICK_REQUEST_CODE = 610;
    private static final int PICK_IMAGE_GALLERY_REQUEST_CODE = 609;
    public static final int CAMERA_STORAGE_REQUEST_CODE = 611;
    public static final int ONLY_CAMERA_REQUEST_CODE = 612;
    public static final int ONLY_STORAGE_REQUEST_CODE = 613;
    public static final int WRONG_PREDICTION = 1;

    private String currentPhotoPath = "";
    private String image = "";
    private Uri imageUri;
    private UiHelper uiHelper = new UiHelper();
    private ImageView imageView;
    private ProgressBar pgsBar;
    private Button wrong;
    TextView predictionText;
    TextView confidenceText;
    static String selectedAnimal;

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, MainActivityImageClassify.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_image_classify);

        findViewById(R.id.selectPictureButton).setOnClickListener(v -> {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                if (uiHelper.checkSelfPermissions(this))
                    uiHelper.showImagePickerDialog(this, this);
        });

        findViewById(R.id.identify).setOnClickListener(v -> {
            uploadToServer(image);
        });

        findViewById(R.id.wrong).setOnClickListener(v -> {
            wrongPrediction(image);
        });

        findViewById(R.id.reset).setOnClickListener(v -> {
            reset();
        });

        imageView = findViewById(R.id.imageView);
        pgsBar = findViewById(R.id.pBar);
        predictionText = findViewById(R.id.prediction);
        confidenceText = findViewById(R.id.confidence);
        wrong = findViewById(R.id.wrong);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == CAMERA_STORAGE_REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED)
                uiHelper.showImagePickerDialog(this, this);
            else if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_DENIED) {
                uiHelper.toast(this, "ImageCropper needs Storage access in order to store your profile picture.");
                finish();
            } else if (grantResults[0] == PackageManager.PERMISSION_DENIED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                uiHelper.toast(this, "ImageCropper needs Camera access in order to take profile picture.");
                finish();
            } else if (grantResults[0] == PackageManager.PERMISSION_DENIED && grantResults[1] == PackageManager.PERMISSION_DENIED) {
                uiHelper.toast(this, "ImageCropper needs Camera and Storage access in order to take profile picture.");
                finish();
            }
        } else if (requestCode == ONLY_CAMERA_REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                uiHelper.showImagePickerDialog(this, this);
            else {
                uiHelper.toast(this, "ImageCropper needs Camera access in order to take profile picture.");
                finish();
            }
        } else if (requestCode == ONLY_STORAGE_REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                uiHelper.showImagePickerDialog(this, this);
            else {
                uiHelper.toast(this, "ImageCropper needs Storage access in order to store your profile picture.");
                finish();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_ACTION_PICK_REQUEST_CODE && resultCode == RESULT_OK) {
            Uri uri = Uri.parse(currentPhotoPath);
            openCropActivity(uri, uri);
        } else if (requestCode == UCrop.REQUEST_CROP && resultCode == RESULT_OK) {
            if (data != null) {
                Uri uri = UCrop.getOutput(data);
                showImage(uri);
            }
        } else if (requestCode == PICK_IMAGE_GALLERY_REQUEST_CODE && resultCode == RESULT_OK && data != null) {
            try {
                Uri sourceUri = data.getData();
                File file = getImageFile();
                Uri destinationUri = Uri.fromFile(file);
                openCropActivity(sourceUri, destinationUri);
            } catch (Exception e) {
                uiHelper.toast(this, e.getMessage());
            }
        } else if (requestCode == WRONG_PREDICTION) {
            if (resultCode == RESULT_OK) {
                uiHelper.toast(this, "Successfully updated.");
            } else {
                uiHelper.toast(this, "Error happened.");
            }
        }
    }

    private void openImagesDocument() {
        Intent pictureIntent = new Intent(Intent.ACTION_GET_CONTENT);
        pictureIntent.setType("image/*");
        pictureIntent.addCategory(Intent.CATEGORY_OPENABLE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            String[] mimeTypes = new String[]{"image/jpeg", "image/png"};
            pictureIntent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        }
        startActivityForResult(Intent.createChooser(pictureIntent, "Select Picture"), PICK_IMAGE_GALLERY_REQUEST_CODE);
    }

    private void showImage(Uri imageUri) {
        try {
            this.imageUri = imageUri;
            File file;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                file = FileUtils.getFile(this, imageUri);
            } else {
                file = new File(currentPhotoPath);
            }
            InputStream inputStream = new FileInputStream(file);
            Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
            imageView.setImageBitmap(bitmap);
        } catch (Exception e) {
            uiHelper.toast(this, "Please select different picture.");
        }
    }

    @Override
    public void onOptionSelected(ImagePickerEnum imagePickerEnum) {
        if (imagePickerEnum == ImagePickerEnum.FROM_CAMERA)
            openCamera();
        else if (imagePickerEnum == ImagePickerEnum.FROM_GALLERY)
            openImagesDocument();
    }

    private void openCamera() {
        Intent pictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File file;
        try {
            file = getImageFile(); // 1
        } catch (Exception e) {
            e.printStackTrace();
            uiHelper.toast(this, e.getMessage());
            return;
        }
        Uri uri;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) // 2
            uri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID.concat(".provider"), file);
        else
            uri = Uri.fromFile(file); // 3
        pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri); // 4
        startActivityForResult(pictureIntent, CAMERA_ACTION_PICK_REQUEST_CODE);
    }

    private File getImageFile() throws IOException {

        String imageFileName = "JPEG_" + System.currentTimeMillis() + "_";
        File storageDir = new File(
                Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_DCIM
                ), "Camera"
        );
        System.out.println(storageDir.getAbsolutePath());
        if (storageDir.exists())
            System.out.println("File exists");
        else
            System.out.println("File not exists");
        File file = File.createTempFile(
                imageFileName, ".jpg", storageDir
        );
        currentPhotoPath = "file:" + file.getAbsolutePath();
        image = file.getAbsolutePath();
        return file;
    }

    private void openCropActivity(Uri sourceUri, Uri destinationUri) {
        UCrop.Options options = new UCrop.Options();
        options.setCircleDimmedLayer(true);
        options.setCropFrameColor(ContextCompat.getColor(this, R.color.colorAccent));
        UCrop.of(sourceUri, destinationUri)
                .withMaxResultSize(500, 500)
                .withAspectRatio(10f, 10f)
                .start(this);
    }

    private void uploadToServer(String filePath) {
        Retrofit retrofit = NetworkClient.getRetrofitClient(this);

        UploadAPI uploadAPIs = retrofit.create(UploadAPI.class);

        //Create a file object using file path
        File file = new File(filePath);

        // Create a request body with file and image media type
        RequestBody fileReqBody = RequestBody.create(MediaType.parse("image/*"), file);

        // Create MultipartBody.Part using file request-body,file name and part name
        MultipartBody.Part part = MultipartBody.Part.createFormData("file", file.getName(), fileReqBody);


        //
        Call call = uploadAPIs.uploadImage(part);
        pgsBar.setVisibility(View.VISIBLE);
        call.enqueue(new Callback<Prediction>() {
            @Override
            public void onResponse(Call<Prediction> call, Response<Prediction> response) {

                parseResponse(response);
                pgsBar.setVisibility(View.GONE);
                predictionText.setVisibility(View.VISIBLE);
                confidenceText.setVisibility(View.VISIBLE);
                wrong.setVisibility(View.VISIBLE);
                Log.d("error", response.body().toString());
            }


            @Override
            public void onFailure(Call call, Throwable t) {

//                Log.e("error",t.getMessage());
                pgsBar.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), "Error Identifying photo", Toast.LENGTH_LONG).show();
            }
        });

    }


    private void parseResponse(Response<Prediction> response) {

        try {

            Toast.makeText(this, response.body().getPercentage(), Toast.LENGTH_LONG);
            TextView predictionText = findViewById(R.id.prediction);
            TextView confidenceText = findViewById(R.id.confidence);

            predictionText.setText(response.body().getPrediction());
            MainActivityImageClassify.selectedAnimal = response.body().getPrediction();
            Log.i("*****", MainActivityImageClassify.selectedAnimal);
            confidenceText.setText("Confidence : " + response.body().getPercentage() + "%");

        } catch (Exception e) {
            Toast.makeText(this, "Error Parsing the Response", Toast.LENGTH_LONG).show();
            Log.e("error", e.getMessage());
        }
    }

    private void wrongPrediction(String image) {
        Intent intent = new Intent(this, WrongPredictionActivity.class);
        intent.putExtra("image", image);
        intent.putExtra("uri", imageUri.toString());
        startActivityForResult(intent, WRONG_PREDICTION);
    }

    private void reset() {
        TextView predictionText = findViewById(R.id.prediction);
        TextView confidenceText = findViewById(R.id.confidence);

        predictionText.setVisibility(View.GONE);
        confidenceText.setVisibility(View.GONE);
        wrong.setVisibility(View.GONE);
        imageView.setImageResource(R.drawable.ic_launcher_background);
    }

    public void onSummaryClick(View view) {
        predictionText.setText("Fish");
        if (!TextUtils.isEmpty(predictionText.getText())) {
            String selectedAnimal = predictionText.getText().toString();
            Intent i = new Intent(getApplicationContext(), Description.class);
            i.putExtra("selectedAnimal", selectedAnimal);
            startActivity(i);
        }else{
            Toast.makeText(this, "No prediction found", Toast.LENGTH_LONG).show();
        }
    }

    public void onGetYouTubeVideosClick(View view) {
        if (!TextUtils.isEmpty(predictionText.getText())) {
            Intent intent = new Intent(MainActivityImageClassify.this, SecondActivityVideoUrls.class);
            intent.putExtra("keyword", predictionText.getText());
            startActivity(intent);
        } else {
            Toast.makeText(this, "No prediction found", Toast.LENGTH_LONG).show();

        }
    }
}