package com.sliit.eazylearn.componentone;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface UploadAPI {
    @Multipart
    @POST("/uploader")
    Call<Prediction> uploadImage(@Part MultipartBody.Part file);

    @Multipart
    @POST("/wrongPrediction")
    Call<ResponseBody> wrongPrediction(@Part MultipartBody.Part file, @Part("type") RequestBody type);
}
