package com.sliit.eazylearn.componentone;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.sliit.eazylearn.R;
import com.sliit.eazylearn.componentone.utils.FileUtils;
import com.sliit.eazylearn.componentone.utils.UiHelper;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class WrongPredictionActivity extends AppCompatActivity {

    private ImageView imageView;
    private ProgressBar pgsBar;
    private String currentPhotoPath;
    private String image;
    private UiHelper uiHelper = new UiHelper();
    private Uri imageUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wrong_prediction);


        imageView = findViewById(R.id.imageView);
        pgsBar =  findViewById(R.id.pBar);

        currentPhotoPath = "file:" + getIntent().getStringExtra("image");
        image = getIntent().getStringExtra("image");
        imageUri = Uri.parse(getIntent().getStringExtra("uri"));
        showImage(imageUri);

        findViewById(R.id.submit).setOnClickListener(v -> {
            uploadToServer(image);
        });
    }

    private void showImage(Uri imageUri) {
        try {
            File file;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                file = FileUtils.getFile(this, imageUri);
            } else {
                file = new File(currentPhotoPath);
            }
            InputStream inputStream = new FileInputStream(file);
            Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
            imageView.setImageBitmap(bitmap);
        } catch (Exception e) {
            uiHelper.toast(this, "Please select different picture.");
        }
    }

    private void uploadToServer(String filePath) {
        Retrofit retrofit = NetworkClient.getRetrofitClient(this);

        UploadAPI uploadAPIs = retrofit.create(UploadAPI.class);

        //Create a file object using file path
        File file = new File(filePath);

        // Create a request body with file and image media type
        RequestBody fileReqBody = RequestBody.create(MediaType.parse("image/*"), file);

        // Create MultipartBody.Part using file request-body,file name and part name
        MultipartBody.Part part = MultipartBody.Part.createFormData("file", file.getName(), fileReqBody);

        Spinner spinner = findViewById(R.id.spinner1);

        String type = String.valueOf(spinner.getSelectedItem());
        RequestBody body = RequestBody.create(MediaType.parse("text/plain"),type);
        //
        Call call = uploadAPIs.wrongPrediction(part,body);
        pgsBar.setVisibility(View.VISIBLE);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {

                pgsBar.setVisibility(View.GONE);
                Log.d("error",response.body().toString());

                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_OK,returnIntent);
                finish();
            }

            @Override
            public void onFailure(Call call, Throwable t) {

                Log.e("error",t.getMessage());
                pgsBar.setVisibility(View.GONE);

                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_CANCELED,returnIntent);
                finish();
                Toast.makeText(getApplicationContext(),"Error submiting photo",Toast.LENGTH_LONG).show();
            }
        });

    }
}
