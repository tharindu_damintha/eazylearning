package com.sliit.eazylearn.componentone.enums;

public enum ImagePickerEnum {

    FROM_GALLERY,
    FROM_CAMERA
}
