package com.sliit.eazylearn.componentone.listeners;

import com.sliit.eazylearn.componentone.enums.ImagePickerEnum;


@FunctionalInterface
public interface IImagePickerLister {
    void onOptionSelected(ImagePickerEnum imagePickerEnum);
}
