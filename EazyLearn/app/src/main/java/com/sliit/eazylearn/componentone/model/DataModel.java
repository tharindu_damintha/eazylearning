package com.sliit.eazylearn.componentone.model;

public class DataModel {
    private String data;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}