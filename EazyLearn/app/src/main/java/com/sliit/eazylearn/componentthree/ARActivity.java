package com.sliit.eazylearn.componentthree;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MotionEvent;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.ar.core.Anchor;
import com.google.ar.core.AugmentedImage;
import com.google.ar.core.Frame;
import com.google.ar.core.TrackingState;
import com.google.ar.core.exceptions.CameraNotAvailableException;
import com.google.ar.sceneform.AnchorNode;
import com.google.ar.sceneform.HitTestResult;
import com.google.ar.sceneform.Node;
import com.google.ar.sceneform.Scene;
import com.google.ar.sceneform.rendering.ModelRenderable;
import com.google.ar.sceneform.ux.ArFragment;
import com.google.ar.sceneform.ux.TransformableNode;
import com.sliit.eazylearn.R;

import java.util.Collection;

public class ARActivity extends AppCompatActivity {


    public static void startActivity(Context context) {
        Intent intent = new Intent(context, ARActivity.class);
        context.startActivity(intent);
    }

    private ArFragment mARFragment;
    private ModelRenderable mObjRenderable;

    private Scene.OnUpdateListener sceneUpdate;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        if (!checkIsSupportedDeviceOrFinish(this)) {
//            return;
//        }

        setContentView(R.layout.activity_ar);
        setARFragment();
    }

    private void setARFragment() {
        mARFragment = (ArFragment) getSupportFragmentManager().findFragmentById(R.id.ux_fragment);
        mARFragment.getPlaneDiscoveryController().hide();
        // When you build a Renderable, Sceneform loads its resources in the background while returning
        // a CompletableFuture. Call thenAccept(), handle(), or check isDone() before calling get().


//        mARFragment.setOnTapArPlaneListener(new BaseArFragment.OnTapArPlaneListener() {
//            @Override
//            public void onTapPlane(HitResult hitResult, Plane plane, MotionEvent motionEvent) {
//                Anchor mAnchor = hitResult.createAnchor();
//                AnchorNode mAnchorNode = new AnchorNode(mAnchor);
//                mAnchorNode.setParent(mARFragment.getArSceneView().getScene());
//
//                // Create the transformable object and add it to the anchor.
//                TransformableNode mARObject = new TransformableNode(mARFragment.getTransformationSystem());
//                mARObject.setParent(mAnchorNode);
//                mARObject.setRenderable(mObjRenderable);
//                mARObject.select();
//            }
//        });
        sceneUpdate = frameTime -> {
            try {
                Frame frame = mARFragment.getArSceneView().getSession().update();
                if (frame != null) {
                    Collection<AugmentedImage> updatedAugmentedImages = frame.getUpdatedTrackables(AugmentedImage.class);
                    for (AugmentedImage img : updatedAugmentedImages) {
                        Anchor mAnchor = null;
                        if (img.getTrackingState() == TrackingState.TRACKING) {
                            mAnchor = img.createAnchor(img.getCenterPose());
                            if (img.getName().equalsIgnoreCase("fish")) {
                                placeObject(mAnchor, R.raw.fish);
                            } else if (img.getName().equalsIgnoreCase("butterfly")) {
                                placeObject(mAnchor, R.raw.butterfly);
                            } else if (img.getName().equalsIgnoreCase("bulath")) {
                                placeObject(mAnchor, R.raw.butterfly);
                            } else if (img.getName().equalsIgnoreCase("dignosor")) {
                                placeObject(mAnchor, R.raw.dignosor);
                            } else if (img.getName().equalsIgnoreCase("jack")) {
                                placeObject(mAnchor, R.raw.jacktree);
                            } else if (img.getName().equalsIgnoreCase("karapincha")) {
                                placeObject(mAnchor, R.raw.karapincha);
                            } else if (img.getName().equalsIgnoreCase("kathurumurunga")) {
                                placeObject(mAnchor, R.raw.butterfly);
                            } else if (img.getName().equalsIgnoreCase("papow")) {
                                placeObject(mAnchor, R.raw.papaya);
                            } else if (img.getName().equalsIgnoreCase("parrot")) {
                                placeObject(mAnchor, R.raw.butterfly);
                            }
                        }
                    }
                }
            } catch (CameraNotAvailableException e) {
                e.printStackTrace();
            }
        };

        if (mARFragment.getArSceneView() != null && mARFragment.getArSceneView().getScene() != null) {
            mARFragment.getArSceneView().getScene().addOnUpdateListener(sceneUpdate);
        }
    }


    private void placeObject(Anchor mAnchor, int image) {

        ModelRenderable.builder()
                .setSource(this, image)
                .build()
                .thenAccept(renderable -> {
                    mObjRenderable = renderable;
                    AnchorNode mAnchorNode = new AnchorNode(mAnchor);
                    mAnchorNode.setParent(mARFragment.getArSceneView().getScene());
                    mAnchorNode.setOnTapListener((hitTestResult, motionEvent) -> mARFragment.getArSceneView().getScene().addOnUpdateListener(sceneUpdate));
                    // Create the transformable object and add it to the anchor.
                    TransformableNode mARObject = new TransformableNode(mARFragment.getTransformationSystem());
                    mARObject.setParent(mAnchorNode);
                    mARObject.setRenderable(mObjRenderable);
                    mARObject.select();
                    mARFragment.getArSceneView().getScene().removeOnUpdateListener(sceneUpdate);
                })
                .exceptionally(
                        throwable -> {
                            Toast toast = Toast.makeText(this, "Unable to load obj renderable", Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                            return null;
                        });

    }
}
