package com.sliit.eazylearn.componentthree;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.google.ar.core.AugmentedImageDatabase;
import com.google.ar.core.Config;
import com.google.ar.core.Session;
import com.google.ar.sceneform.ux.ArFragment;

import java.io.IOException;
import java.io.InputStream;

public class MyARFragment extends ArFragment {
    private AugmentedImageDatabase imageDatabase;

    @Override
    protected Config getSessionConfiguration(Session session) {
        getPlaneDiscoveryController().setInstructionView(null);
        Config config = super.getSessionConfiguration(session);
        config.setUpdateMode(Config.UpdateMode.LATEST_CAMERA_IMAGE);
        config.setFocusMode(Config.FocusMode.AUTO);
        imageDatabase = new AugmentedImageDatabase(session);

        addToDb("cow.jpg", "cow");
        addToDb("bulath.jpg","bulath");
        addToDb("butterfly.jpg","butterfly");
        addToDb("dignosor.jpg","dignosor");
        addToDb("fish.jpg","fish");
        addToDb("jack.jpg","jack");
        addToDb("karapincha.jpg","karapincha");
        addToDb("kathurumurunga.jpg","kathurumurunga");
        addToDb("papow.jpg","papow");
        addToDb("parrot.jpg","parrot");

        config.setAugmentedImageDatabase(imageDatabase);
        session.configure(config);
        return config;

    }

    private void addToDb(String imageName, String id){
        Bitmap bitmap  = null;
        try {
            InputStream inputStream = getActivity().getAssets().open(imageName);
            bitmap = BitmapFactory.decodeStream(inputStream);
        } catch (IOException ex) {
            Log.e("AR", "I/O exception loading augmented image bitmap.", ex);
        }

        if(bitmap!= null) {
            int index = imageDatabase.addImage(id, bitmap);
        }
    }
}
