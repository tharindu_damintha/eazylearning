package com.sliit.eazylearn.componenttwo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.sliit.eazylearn.R;


public class MainActivityVideoUrls extends AppCompatActivity {
    public static void startActivity(Context context) {
        Intent intent = new Intent(context, MainActivityVideoUrls.class);
        context.startActivity(intent);
    }
    private EditText keyword;
    private Button processUrl;
    private TextView textView1;

    private String errorMsg = "Please provide a keyword !";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_video_urls);

        keyword = (EditText)findViewById(R.id.editText);
        processUrl = (Button)findViewById(R.id.btnSubmit);
        textView1 = (TextView)findViewById(R.id.tvError);

        processUrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validateKeyword(keyword.getText().toString());
            }
        });
    }

    private void validateKeyword(String keyword) {
        if((keyword.equals(null))  || (keyword.equals(""))) {
            textView1.setText(errorMsg);
        } else {
            textView1.setText("");
            Intent intent = new Intent(MainActivityVideoUrls.this, SecondActivityVideoUrls.class);
            intent.putExtra("keyword",keyword);
            startActivity(intent);
        }
    }
}
