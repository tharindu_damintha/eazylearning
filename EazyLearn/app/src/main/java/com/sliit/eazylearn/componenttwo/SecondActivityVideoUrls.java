package com.sliit.eazylearn.componenttwo;

import androidx.appcompat.app.AppCompatActivity;

import android.app.VoiceInteractor;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.sliit.eazylearn.R;

import org.json.*;

import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class SecondActivityVideoUrls extends AppCompatActivity {

    ListView listView;
    ProgressBar progressBar;
    ArrayList<String> arrayList;
    ArrayList<String> arrayList2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second_video_urls);

        listView = (ListView)findViewById(R.id.listView);
        progressBar = findViewById(R.id.progressBar);

        String value = getIntent().getStringExtra("keyword");


        final String URL = "http://192.168.8.103:5002/getPrediction/"+value+" educational";

        RequestQueue requestQueue =Volley.newRequestQueue(this);

        arrayList = new ArrayList<>();
        arrayList2 = new ArrayList<>();
        progressBar.setVisibility(View.VISIBLE);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(
                URL,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        progressBar.setVisibility(View.GONE);
                        Log.e("Rest Response",response.toString());

//                        Toast.makeText(SecondActivity.this, response.toString(), Toast.LENGTH_SHORT).show();
                        for (int i = 0; i < response.length(); i++) {
                            try {

                                JSONObject leagueData = response.getJSONObject(i);
//                                Toast.makeText(SecondActivity.this, leagueData.toString(), Toast.LENGTH_SHORT).show();
                                String title = leagueData.getString("title");
                                String description = leagueData.getString("description");
                                String prediction = leagueData.getString("prediction");
                                String url = leagueData.getString("link");

                                Log.e("title",title);
//                                Toast.makeText(SecondActivity.this, title, Toast.LENGTH_SHORT).show();
                                arrayList.add(title);
                                arrayList2.add(url);


                            }
                            catch (JSONException e) {
                                Log.e("MYAPP", "unexpected JSON exception", e);
                                // Do something to recover ... or kill the app.
                            }
                        }

//                      Toast.makeText(SecondActivity.this, arrayList.toString(), Toast.LENGTH_SHORT).show();
                        ArrayAdapter arrayAdapter = new ArrayAdapter(SecondActivityVideoUrls.this,android.R.layout.simple_list_item_1,arrayList);
                        listView.setAdapter(arrayAdapter);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressBar.setVisibility(View.GONE);
                        Log.e("Rest Response",error.toString());
                    }
                }

        );


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String a = arrayList2.get(i);
                Toast.makeText(SecondActivityVideoUrls.this,a, Toast.LENGTH_SHORT).show();

                String url = "http://www.youtube.com/watch?v=hvLznzKDjlk";

                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(a));
                startActivity(intent);
            }
        });

        requestQueue.add(jsonArrayRequest);

    }

}
